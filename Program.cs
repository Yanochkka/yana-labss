﻿using System;
using Делегаты_18_вариант;

namespace События_вариант_18
{
            public class Статьи
       {
            public int Rating { get; set; }
            public int Number_of_words { get; set; }
             
        }
    public class Автор
        {
            public delegate void АвторHandler(string message);
            public event АвторHandler Notify;
            public string FirstName { get; set; }
            public string LastName { get; set; }
            private int _wage;
            public int Wage
            {
                get
                {
                    return _wage;
                }
                set
                {
                    WageChange(value);
                }
            }

            public void WageChange(int value)
            {
                _wage = value;
                Notify?.Invoke($"Ваша зарплата: {Wage}");   // Вызов события
            }
        }

    }

            class Program
{
            static Автор x;
            static void Main(string[] args)
    {
            x = new Автор();
            x.Notify += DisplayMessage;
            x.FirstName = "Ivan";
            x.LastName = "Ivanov";
            x.Wage = 100000;
            Console.ReadKey();
            x.Wage = 50;
            Console.ReadKey();
           
    }
             private static void DisplayMessage(string message)
    {
        Console.WriteLine(message);
    }
}


