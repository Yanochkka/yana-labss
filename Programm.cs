﻿using System;

namespace Делегаты_18_вариант
{

    public class Автор
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Wage { get; set; }
        public delegate void АвторHandler(string message);
        public event АвторHandler Notify;
        private int _wage;
        
    }
    public class Статьи
    {
        public int Rating { get; set; }
        public int Number_of_words { get; set; }
    }
    public class Ссылки
    {
        public int Number_of_links { get; set; }
    }

    class Program
    {
        delegate void Information(Автор x);
        delegate void Message(Статьи z);
        delegate void Messag(Ссылки y);
        static Автор x;
        static Статьи z;
        static Ссылки y;
        static void Main(string[] args)
        {
            Messag messs;
            y = new Ссылки();
            y.Number_of_links = 5;
            int NumberOfMethod;
            NumberOfMethod = Convert.ToInt32(Console.ReadLine());
            switch (NumberOfMethod)
            {
                case 1:
                    Console.WriteLine("Назначим Method1");
                    messs = Method_1_1_;
                    break;
                default:
                
                messs = Method_1_1_;
                    break;

            }
            Console.WriteLine("Вызов делегата!");
            messs(y);
            Console.ReadKey();
            Message mess;
            z = new Статьи();
            z.Rating = 100;
            z.Number_of_words = 1000;
            if (z.Rating > 85)
            {
                mess = Method1_1;
            }
            else
            {
                mess = Method2_2;
            }
            mess(z);
            Console.ReadKey();


            Information mes;
            x = new Автор();
            x.FirstName = "Саша";
            x.LastName = "Иванов";
            x.Wage = 10000;

            NumberOfMethod = Convert.ToInt32(Console.ReadLine());
            switch (NumberOfMethod)
            {
                case 1:
                    Console.WriteLine("Назначим Method1");
                    mes = Method1;
                    break;
                case 2:
                    Console.WriteLine("Назначим Method2");
                    mes = Method2;
                    break;
                case 3:
                    Console.WriteLine("Назначим Method3");
                    mes = Method3;
                    break;
                default:
                    Console.WriteLine("Вызовем все методы");
                    mes = Method1;
                    mes += Method2;
                    mes += Method3;
                    break;
            }
            Console.WriteLine("Вызов делегата!");
            mes(x);
            Console.ReadKey();
        }
        private static void Method1(Автор x)
        {
            Console.WriteLine("Рады приветствовать Вас!");
        }
        private static void Method2(Автор x)
        {
            Console.WriteLine("Добро пожаловать" + x.FirstName + " " + x.LastName);
        }
        private static void Method3(Автор x)
        {
            Console.WriteLine("Добро пожаловать" + x.FirstName + " " + x.LastName);
            Console.WriteLine("Ваша зарплата: " + x.Wage);
        }
        private static void Method1_1(Статьи z)
        {
            Console.WriteLine("Высокий рейтинг статьи!" + "Слов в статье: " + z.Number_of_words);
        }
        private static void Method2_2(Статьи z)
        {
            Console.WriteLine("Низкий рейтинг статьи!" + "Слов в статье: " + z.Number_of_words);
        }
        private static void Method_1_1_(Ссылки y)
        {

        }
    }
   

}


