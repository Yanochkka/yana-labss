﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
	public class Author
	{
		private int age;
		public string Name { get; set; }
		public int Age
		{
			get { return age; }
			set
			{
				if (value < 18)
				{
					throw new Exception("Лицам до 18 работа в редакции запрещена");
				}
				else
				{
					age = value;

				}
			}
		}
	}
}