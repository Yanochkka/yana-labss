﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Сериализация_вариант_18
{
    [Serializable]
    public class Автор
    {
        public string Name { get; set; }
        public string FirstName { get;}
        public int Age { get; set; }
        public Статьи Статьи { get; private set; } 
       

        public Автор()
        { }

        public Автор(string name, int age, Статьи статьи, string firstname)
        {
            Name = name;
            Age = age;
            Статьи = статьи;
            FirstName = firstname;
        }
    }

    [Serializable]
    public class Статьи
    {
        public string Name { get; set; }

        // стандартный конструктор без параметров
        public Статьи() { }

        public Статьи(string name)
        {
            Name = name;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Автор person1 = new Автор("Миша", 29, new Статьи("Microsoft"), "Иванов");
            Автор person2 = new Автор("Саша", 25, new Статьи("Apple"), "Петров");
            Автор[] people = new Автор[] { person1, person2 };

            XmlSerializer formatter = new XmlSerializer(typeof(Автор[]));

            using (FileStream fs = new FileStream("people.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, people);
            }

            using (FileStream fs = new FileStream("people.xml", FileMode.OpenOrCreate))
            {
                Автор[] newpeople = (Автор[])formatter.Deserialize(fs);

                foreach (Автор p in newpeople)
                {
                    Console.WriteLine($"Имя: {p.Name} --- Возраст: {p.Age} --- Статья: {p.Статьи.Name}");
                }
            }
            Console.ReadLine();
        }
    }
}
